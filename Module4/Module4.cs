﻿using System;
using System.ComponentModel;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            //comment
        }


        public int Task_1_A(int[] array)
        {
            try
            {
                int max = array[0];
                foreach (int item in array)
                {
                    if (max < item)
                    {
                        max = item;
                    }
                }
                return max;
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

        public int Task_1_B(int[] array)
        {
            try
            {
                int max = array[0];
                foreach (int item in array)
                {
                    if (max > item)
                    {
                        max = item;
                    }
                }
                return max;
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

        public int Task_1_C(int[] array)
        {
            try
            {
                if (array.Length == 0) throw new ArgumentNullException(nameof(array));
                int max = 0;
                foreach (int item in array)
                {
                    max += item;
                }
                return max;
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

        public int Task_1_D(int[] array)
        {
            try
            {
                int max = array[0], min = array[0];
                foreach (int item in array)
                {
                    if (max < item)
                    {
                        max = item;
                    }
                    if (min > item)
                    {
                        min = item;
                    }
                }
                return max - min;
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

        public void Task_1_E(int[] array)
        {
            try
            {
                int max = array[0], min = array[0];
                foreach (int item in array)
                {
                    if (max < item)
                    {
                        max = item;
                    }
                    if (min > item)
                    {
                        min = item;
                    }
                }
                for (int i = 0; i < array.Length; i++)
                {
                    if (i % 2 == 1)
                    {
                        array[i] -= min;
                    }
                    else
                    {
                        array[i] += max;
                    }
                }
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return String.Concat(a, b);
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] mas;
            if (a.Length >= b.Length)
            {
                mas = a;
                for (int i = 0; i < b.Length; i++)
                {
                    mas[i] += b[i];
                }
            }
            else
            {
                mas = b;
                for (int i = 0; i < a.Length; i++)
                {
                    mas[i] += a[i];
                }
            }
            return mas;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0) throw new ArgumentException("error");
            length = 2 * radius * Math.PI;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            maxItem = array[0];
            minItem = array[0];
            sumOfItems = 0;
            foreach (int item in array)
            {
                if (maxItem < item)
                {
                    maxItem = item;
                }
                if (minItem > item)
                {
                    minItem = item;
                }
                sumOfItems += item;
            }
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return (numbers.Item1, numbers.Item2, numbers.Item3);
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0) throw new ArgumentException("error");
            double length = Math.PI * 2 * radius;
            double square = Math.PI * Math.Pow(radius, 2);
            return (length, square);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array.Length == 0) throw new ArgumentException("error");
            int min = array[0], max = array[0], sum = 0;
            foreach (int item in array)
            {
                if (max < item)
                {
                    max = item;
                }
                if (min > item)
                {
                    min = item;
                }
                sum += item;
            }
            return (min, max, sum);
        }

        public void Task_5(int[] array)
        {
            if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array.Length == 0) throw new ArgumentNullException(nameof(array));
            Array.Sort(array);
            if (direction == SortDirection.Descending)
            {
                int temp;
                for (int i = 0; i < array.Length / 2; i++)
                {
                    temp = array[i];
                    array[i] = array[array.Length - i - 1];
                    array[array.Length - i - 1] = temp;
                }
            }
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            double c = (x2 + x1) / 2;
            if (func(c) == 0 || x1 + x2 < e) return c;
            if (func(c) * func(x1) < 0)
            {
                return Task_7(func, x1, c, e, result);
            }
            else
            {
                return Task_7(func, c, x2, e, result);
            }
        }
    }
}
